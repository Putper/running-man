class Character extends Sprite
{
    /**
     * Extends parent's constructor and adds extra parameters needed for movement
     * @param {Number} speed
     * @param {Number} posX 
     * @param {Number} posY 
     * @param {Number} left 
     * @param {Number} right 
     * @param {Number} up 
     * @param {Number} down 
     * @param {Number} ticksPerFrame 
     * @param {Number} numberOfFrames 
     */
    constructor( speed, posX, posY, left, right, up, down, ticksPerFrame = 0, numberOfFrames = 1 )
    {
        // call sprite's constructor
        super( posX, posY, ticksPerFrame, numberOfFrames );

        this.speed = speed || 0;   // Moving speed of the sprite
        this.movingRight = 0;       // If the sprite moving right (0 is still, -1 is backwards, 1 is forward)
        this.movingDown = 0;        // if the sprite is moving down (0 is still, -1 is up, 1 is down)
        
        this.registerControls(this, left, right, up, down);
    }


    /**
     * Update the sprite's mask and position.
     */
    update(map=false)
    {
        // Extend parent update
        Sprite.prototype.update.apply(this, arguments);

        this.posX += (this.movingRight*this.speed);
        this.posY += (this.movingDown*this.speed);

        if(map) this.checkPos(map);
    }
    
    
    /**
     * visually render the sprite
     * @param {Canvas2dContext} context 
     * @param {Number} row
     * @param {Number} width 
     * @param {Number} height 
     * @param {Image} image
     * @param {Number} frameIndex Change this if you only want to show one specific frame
     */
    draw( context, row, width, height, image, frameIndex = false )
    {

        // Set character's spritesheet row based on movement
        if( !this.movingDown && !this.movingRight )
            frameIndex = 1; row = 0;

        if(this.movingDown > 0)    row = 0;
        if(this.movingDown < 0)    row = 200;
        if(this.movingRight > 0)   row = 100;
        if(this.movingRight < 0)   row = 300;

        // Extend parent draw
        Sprite.prototype.draw.apply(this, [context, row, width, height, image, frameIndex ]);
    }
    
    
    /**
     * Define controls for the movement
     * @param {Character} char 
     * @param {Number} left 
     * @param {Number} right 
     * @param {Number} up 
     * @param {Number} down 
     */
    registerControls( char, left, right, up, down )
    {
        // change character movement based on key presses
        document.onkeydown = function(e)
        {
            switch( e.keyCode )
            {
                // Left arrow
                case 37:
                    char.movingRight = -1;
                    break;
                
                // Right arrow
                case 39:
                    char.movingRight = 1;
                    break;

                // Up arrow
                case 38:
                    char.movingDown = -1;
                    break;

                // Down arrow
                case 40:
                    char.movingDown = 1;
                    break;
            }
        }
        document.onkeyup = function(e)
        {
            switch( e.keyCode )
            {
                // Left arrow
                case 37:
                    if(char.movingRight < 0) char.movingRight = 0;
                    break;
                
                // Right arrow
                case 39:
                    if(char.movingRight > 0) char.movingRight = 0;
                    break;

                // Up arrow
                case 38:
                    if(char.movingDown < 0) char.movingDown = 0;
                    break;

                // Down arrow
                case 40:
                    if(char.movingDown > 0) char.movingDown = 0;
                    break;
            }
        }
    }


    checkPos(map)
    {
        let playerPos = map.getPlayer();
        
        // If moving left of the current tile
        if( this.movingRight < 0 && this.posX < 0 )
        {
            let tileLeft = map.level[ playerPos[0] ][ playerPos[1]-1];

            // If there's an open spot on the left
            if( tileLeft == 0 )
            {
                this.posX += c.width;   // Teleport to right side
                map.movePlayer( playerPos[0], playerPos[1]-1 );
            }
            else if( tileLeft == 3)
            {
                endLevel(map, this);
            }
            else    // If there's a wall, stop player from moving left
                this.posX = 0;
        }
                
        // If moving right of the current tile
        if( this.movingRight > 0 && this.posX > c.width - this.frameWidth )
        {
            let tileRight = map.level[ playerPos[0] ][ playerPos[1]+1 ];

            // If there's an open spot on the right
            if( tileRight == 0 )  
            {
                this.posX = this.posX - c.width;  // Teleport to right side
                map.movePlayer( playerPos[0], playerPos[1]+1 );
            }
            else if ( tileRight == 3 )
            {
                endLevel(map, this);
            }
            else    // If there's a wall, stop player from moving right
                this.posX = c.width - this.frameWidth;       
        }
        
        // If moving top of the current tile
        if( this.movingDown < 0 && this.posY < 0 )
        {
            let tileTop = map.level[ playerPos[0]-1 ][ playerPos[1] ];

            // If there's an open spot upwards
            if( tileTop == 0 )  
            {
                this.posY += c.height;  // Teleport to bottom side
                map.movePlayer( playerPos[0]-1, playerPos[1] );
            }
            else if( tileTop == 3 )
            {
                endLevel(map, this);
            }
            else    // If there's a wall, stop player from moving upwards
                this.posY = 0;       
        }
        
        // If moving Down of the current tile
        if( this.movingDown > 0 && this.posY > c.height - this.frameHeight )
        {
            let tileDown = map.level[ playerPos[0]+1 ][ playerPos[1] ];

            // If there's an open spot downwards
            if( tileDown == 0 )  
            {
                this.posY = this.posY - c.height;  // Teleport to top side
                map.movePlayer( playerPos[0]+1, playerPos[1] );
            }
            else if ( tileDown == 3 )
            {
                endLevel(map, this);
            }
            else    // If there's a wall, stop player from moving downwards
                this.posY = c.height - this.frameHeight;       
        }
    }
}
