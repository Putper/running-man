function endLevel(map, player)
{
    window.alert("Next level unlocked!");
    map.level = map.level_num + 1;

    player.posX = c.height/2 - player.frameWidth;
    player.posY = c.height/2 - player.frameHeight;
    player.movingRight = 0;
    player.movingDown = 0;
}

/**
 * Main loop of the game
 */
function loop()
{
    ctx.clearRect(0, 0, c.width, c.height);

    // Tell browser we want to perform an animation
    window.requestAnimationFrame(loop);

    // Update & render sprites
    character.update(map);
    character.draw(
        ctx,
        100,
        600,
        100,
        charImage
    );
    
    // Draw minimap
    map.draw(ctx);
}
loop();
