// Get canvas & context
let c = document.getElementById("canvas");
let ctx = c.getContext("2d");


// Define all levels
levels = [
    "https://api.myjson.com/bins/g2xvj",
    "https://api.myjson.com/bins/iitgf",
    "https://api.myjson.com/bins/13jgnj"
];
// let lvl1 = jsonRequest("https://api.myjson.com/bins/g2xvj");
// let lvl2 = jsonRequest("https://api.myjson.com/bins/iitgf");


// Define character
let charImage = new Image();
charImage.src = "images/trump.png";

let character = new Character(
    8,      // movement speed
    100,    // X pos
    100,    // Y Pos
    37,     // Left arrow
    39,     // Right Arrow
    38,     // Up arrow
    40,     // Down arrow
    5,     // Animation speed
    6
);


// define minimap
let map = new Map(levels);