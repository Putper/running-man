/**
 * A map generally used for the minimap
 */
class Map {
    constructor(levels, level = 0)
    {
        this.levels = levels;
        this.level = level;
        this._level_num = level;
    }


    /**
     * Generate a visual representation of the map
     * @param {Canvas2dContext} context
     * @param {Object} level A level already converted to an object
     * @param {Number} size Number in percentage 
     */
    draw(context, size=25)
    {
        // Calculate the size of the map based on given size percentage
        this.mapSize = parseInt( c.width / 100 * size );
        // Calculate where the map must be positioned to end up in the top right corner
        this.mapPos = parseInt( c.width - this.mapSize );
        
        // Calculate width and height of each tile
        this.tileWidth = parseInt( this.mapSize / this._level[0].length );
        this.tileHeight = parseInt( this.mapSize / this._level.length );


        // For every row in the map
        for( let i=0; i < this._level.length; i++ )
        {
            // For every column
            for( let j=0; j < this._level[i].length; j++ )
            {
                context.beginPath();

                let drawCircle;

                // Switch case that decides colour of the tile
                switch( this._level[i][j] )
                {
                    case 0:
                        context.fillStyle = "white";
                        break;

                    case 1:
                        context.fillStyle = "#dc3d20";
                        break;

                    case 2:
                        context.fillStyle = "white";
                        drawCircle = true;
                        break;

                    case 3:
                        context.fillStyle = "green";
                        break;
                }
                
                // Draw the tile
                context.fillRect(
                    this.mapPos+(j*this.tileWidth), 
                    i*this.tileHeight, 
                    this.tileWidth, 
                    this.tileHeight
                );
               
                
                // Draw Character in map
                if(drawCircle)
                {
                    drawCircle = false;

                    context.closePath();
                    context.beginPath();

                    context.fillStyle = "blue";

                    context.arc(
                        this.mapPos + (j*this.tileWidth) + (this.tileWidth/2),
                        i*this.tileHeight + (this.tileHeight/2),
                        (this.tileHeight+this.tileWidth)/4,
                        0,
                        2*Math.PI
                    );
                    
                    context.fill();
                }

                context.closePath();
            }
        }
    }


    getPlayer()
    {
        for( let i=0; i < this._level.length; i++ )
        {
            for( let j=0; j < this._level[1].length; j++ )
            {
                if( this._level[i][j] == "2" )
                {
                    
                    return [i, j];
                }
            }
        }
    }
    

    movePlayer(x, y)
    {
        let playerPos = this.getPlayer();

        this._level[ playerPos[0] ][ playerPos[1] ] = 0;

        this._level[x][y] = 2;
    }
    
    /**
     * Set the level
     * @param {String} level url to a map
     */
    set level(level)
    {
        this.level_num = level;
        this._level = jsonRequest(this.levels[level]);
    }

    
    get level()
    {
        return this._level;
    }
    
    
    get level_num()
    {
        return this._level_num;
    }
    
    
    set level_num(number)
    {
        this._level_num = number;
    }
}
