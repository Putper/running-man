class Sprite
{
    /**
     * Sets given parameters to class which will later be needed
     * @param {Number} posX X position on canvas
     * @param {Number} posY Y position on cavas
     * @param {Number} ticksPerFrame How fast the animation goes
     * @param {Number} numberOfFrames Amount of frames the animation has
     */
    constructor( posX, posY, ticksPerFrame = 0, numberOfFrames = 1 )
    {
        this.frameIndex = 0;                          // Variable used to count which frame of a spritesheet is currently being used
        this.tickCount = 0;                           // Counter used for TicksPerFrame
        this.ticksPerFrame = ticksPerFrame;      // how many updates have to be called to go to the next frame
        this.numberOfFrames = numberOfFrames;    // How many frames a spritesheet has, default is 1 because regular images only have 1

        // Set current options with parameters given
        this.posX = posX;          // X position of the sprite
        this.posY = posY;          // Y position of the sprite
    }


    /**
     * update the sprite's mask
     */
    update()
    {
        this.tickCount+=1; // Add 1 to tick

        // If the counter reached the max per frame, reset the counter and change the frame
        if( this.tickCount > this.ticksPerFrame )
        {
            this.tickCount = 0;

            // If current frame isn't the last, set it to next frame
            // If it is the last, set it to the first frame.
            this.frameIndex < this.numberOfFrames-1 ? this.frameIndex+=1 : this.frameIndex = 0;
        }
    }
    
    
    /**
     * visually render the sprite
     * @param {Canvas2dContext} context 
     * @param {Number} row at which height the sprite begins
     * @param {Number} width Width of the spritesheet
     * @param {Number} height Height of the frame that has to be shown
     * @param {Image} image Image of the spritesheet
     * @param {Number} frameIndex Change this if you only want to show one specific frame
     */
    draw( context, row, width, height, image, frameIndex = false )
    {
        this.frameWidth = width / this.numberOfFrames;
        this.frameHeight = height;
        
        if(frameIndex !== false) this.frameIndex = frameIndex

        // Remove old sprite
        // context.clearRect(this.posX, this.posY, width/this.numberOfFrames, height);

        // Draw new sprite
        context.drawImage(
            image,                                         // Image to be drawn
            this.frameIndex * width / this.numberOfFrames, // X position of frame
            row,                                           // Y position of frame
            width / this.numberOfFrames,                   // width of frame that has to be shown
            height,                                        // Height of frame that has to be shown
            this.posX,                                         // X position on canvas
            this.posY,                                         // Y position on canvas
            width / this.numberOfFrames,                   // stretch width of frame
            height,                                        // stretch height of frame
        );

        // Reset the transform
        context.setTransform(1,0,0,1,0,0);
    };
}
