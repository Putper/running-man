/**
 * Gets json from url and returns as object
 * @param {String} url 
 * @return {Object}
 */
function jsonRequest(url)
{
    let httpreq = new XMLHttpRequest();
    httpreq.open("GET", url, false);
    httpreq.send(null);
    return JSON.parse(httpreq.responseText);
}
